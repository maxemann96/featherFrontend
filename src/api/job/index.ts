import { BackgroundJob } from '../../entities';
import { axios, runCatching } from '../util';

export type ErrorResult = {
  error: 'not_found' | 'fetch' | 'backend';
};

export function isErrorResult(
  result: BackgroundJob | ErrorResult | undefined
): result is ErrorResult {
  return typeof result === 'object' && 'error' in result;
}

export async function getJob(jobId: string): Promise<BackgroundJob | undefined> {
  return await runCatching<BackgroundJob>(() =>
    axios.get(`/background_job/${jobId}`, {
      validateStatus: (it) => it == 200 || it == 404 || it == 400,
    })
  ).then((it) => (it.status == 200 ? it.data : undefined));
}
