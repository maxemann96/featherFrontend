import { Session } from '../../entities';
import { axios, runCatching } from '../util';

export async function logout(): Promise<{ redirectUrl: string } | 'no_session'> {
  return await runCatching<any>(() =>
    axios.delete('/session', {
      validateStatus: (it) => it == 404 || it == 200,
    })
  ).then((it) => (it.status == 200 ? { redirectUrl: it.data } : 'no_session'));
}

export async function login(authorizer: string, body: any): Promise<Session | undefined> {
  return await runCatching<Session>(() =>
    axios.post(`/authorizer/${authorizer}`, JSON.stringify(body), {
      validateStatus: (it) => it == 201 || it == 401,
    })
  ).then((it) => (it.status == 201 ? it.data : undefined));
}
