import { axios, runCatching } from '../util';

export type GroupResponse = {
  id: number;
  description: string;
  name: string;
  groupMembers: number[];
  ownedGroups: number[];
  ownerGroups: number[];
  owners: number[];
  parentGroups: number[];
  userMembers: number[];
};

export async function getAllGroups(): Promise<GroupResponse[]> {
  return await runCatching<GroupResponse[]>(() => axios.get('/groups')).then((it) => it.data);
}
