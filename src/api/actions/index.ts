import { axios, runCatching } from '../util';

export type ActionResponse = {
  name: string;
  description: string;
};

export type ActionsByUsersResponse = {
  userId: number;
  actions: ActionResponse[];
};

export async function getAllOpenActions(): Promise<ActionsByUsersResponse[]> {
  return await runCatching<ActionsByUsersResponse[]>(() => axios.get('/actions')).then(
    (it) => it.data
  );
}
