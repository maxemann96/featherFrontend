import { axios, runCatching } from '../util';

export type TrialRequest = {
  id: number;
  username: string;
  displayName: string;
  extensions: number;
  started: string;
};

export async function getSupportMembershipVerificationStatus(): Promise<{
  data: string;
  lastUpdated?: string;
}> {
  return await runCatching<string>(() =>
    axios.get<string>(`bindings/iog/users/verify`, {
      validateStatus: (it) => it == 200 || it == 403,
    })
  ).then((it) => {
    const lastModified = it.headers['last-modified'];
    const formattedDate = lastModified ? new Date(lastModified).toLocaleString() : undefined;

    return {
      data: it.data,
      lastUpdated: formattedDate,
    };
  });
}

export async function getAllTrialRequests(): Promise<TrialRequest[]> {
  return await runCatching(() =>
    axios.get<{ isAdmin: boolean; openRequests: TrialRequest[] }>(
      `bindings/iog/users/getAllTrialRequests`,
      {
        validateStatus: (it) => it == 200,
      }
    )
  ).then((it) =>
    it.data.openRequests.sort((o1, o2) => o1.displayName.localeCompare(o2.displayName))
  );
}

export async function processTrialRequests(accept: boolean, requestId: number[]): Promise<{}> {
  return await runCatching(() =>
    axios.post<{ isAdmin: boolean; openRequests: TrialRequest[] }>(
      `bindings/iog/users/processTrialRequest`,
      {
        accept,
        requestId,
      },
      {
        validateStatus: (it) => it == 204,
      }
    )
  ).then((it) => it.data);
}
