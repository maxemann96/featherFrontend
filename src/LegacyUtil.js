/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import * as util from "./util"

export default {
  randomInteger: function (min, max) {
    // returns a random number between min and max (both included)
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  getSortedUsersById: function (ids, users) {
    let resolved = ids.map((x) => users.get(x)).filter((x) => x);
    resolved.sort((o1, o2) => (o1.firstname + o1.surname).localeCompare(o2.firstname + o2.surname));
    return resolved;
  },
  getSortedGroupsById: function (ids, groups) {
    return util.groups.getSortedGroupsById(ids, groups)
  },
  getImplicitUserIds: function (group, mapOfAllGroups) {
    let ids = group.userMembers;
    group.groupMembers
      .map((groupId) => mapOfAllGroups.get(groupId))
      .forEach((group) => (ids = ids.concat(this.getImplicitUserIds(group, mapOfAllGroups))));
    return [...new Set(ids)];
  },
  getImplicitOwnerIds: function (group, mapOfAllGroups) {
    let ids = group.owners;
    if (group.dnParent) {
      const parentGroup = mapOfAllGroups.get(group.dnParent);
      ids = ids.concat(this.getImplicitOwnerIds(parentGroup, mapOfAllGroups));
    }
    group.ownerGroups
      .map((groupId) => mapOfAllGroups.get(groupId))
      .forEach((ownerGroup) => (ids = ids.concat(ownerGroup.userMembers)));
    return [...new Set(ids)];
  },
  allImplicitGroups: function (groupIds, mapOfAllGroups) {
    if (!groupIds || groupIds.length == 0) return [];
    let combinedGroupIds = [...groupIds];
    const groups = groupIds.map((groupId) => mapOfAllGroups.get(groupId));
    groups
      .filter((group) => group.parentGroups)
      .forEach(
        (group) =>
          (combinedGroupIds = combinedGroupIds.concat(
            this.allImplicitGroups(group.parentGroups, mapOfAllGroups)
          ))
      );
    groups
      .filter((group) => group.dnParent)
      .forEach(
        (group) =>
          (combinedGroupIds = combinedGroupIds.concat(
            this.allImplicitGroups(group.dnParent, mapOfAllGroups)
          ))
      );
    return [...new Set(combinedGroupIds)];
  },
  getImplicitGroupmembershipsForUser: function (user, mapOfAllGroups) {
    return this.allImplicitGroups(user.groups, mapOfAllGroups);
  },
  getImplicitGroupmembershipsForGroup: function (group, mapOfAllGroups) {
    return this.allImplicitGroups([group.id], mapOfAllGroups);
  },
  userIdentifier: function (user, viewingUser) {
    // NOTE: never show self-chosen <username>
    if (viewingUser.permissions.includes('ADMIN') || viewingUser.ownedGroups.length !== 0) {
      return user.displayName !== `${user.firstname} ${user.surname}`
        ? `${user.firstname} ${user.surname} (${user.displayName})`
        : user.displayName;
    } else {
      return user.displayName;
    }
  },
  userUniqueIdentifier: function (user) {
    return user.displayName + ' (' + user.username + ')';
  },
  harmonizeName: function (name) {
    let result = name.toLowerCase();
    result = result.replace(/ß/g, 'ss');
    result = result.replace(/ä/g, 'ae');
    result = result.replace(/ö/g, 'oe');
    result = result.replace(/ü/g, 'ue');
    return result;
  },
  displayNameSuggestion: function (user) {
    let harmonizedFirstname = this.harmonizeName(user.firstname);
    let harmonizedSurname = this.harmonizeName(user.surname);
    return `${harmonizedFirstname}.${harmonizedSurname}`;
  },
  isValidDate: function (d) {
    return d instanceof Date && !isNaN(d);
  },
  sha256: async function (message) {
    // encode as UTF-8
    const msgBuffer = new TextEncoder().encode(message);

    // hash the message
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);

    // convert ArrayBuffer to Array
    const hashArray = Array.from(new Uint8Array(hashBuffer));

    // convert bytes to hex string
    const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join('');
    return hashHex;
  },
  longestItem: function (vals) {
    return vals.reduce((a, x) => (x.length > a.length ? x : a));
  },
  hashNameBirthday: async function (firstName, lastName, birthday) {
    if (!this.isValidDate(birthday)) {
      return await this.sha256('');
    }
    // birthday format: YYYYMMDD
    const formattedBirthday = birthday.toISOString().slice(0, 10).replace(/-/g, '');
    /* To simplify cross-checking and accomodate for minor inconsistencies between
     * FundraisingBox and DKP, only take the first part of firstname,
     * and the longest part of lastname. Splitted at whitespace and dashes.
     * "Harmonize" umlauts and convert to lowercase. */
    let conditionedFirstName = firstName.split(/[\s-]/)[0];
    conditionedFirstName = this.harmonizeName(conditionedFirstName);
    let conditionedLastName = this.longestItem(lastName.split(/[\s-]/));
    conditionedLastName = this.harmonizeName(conditionedLastName);
    return await this.sha256(`${conditionedFirstName}${conditionedLastName}${formattedBirthday}`);
  },
  hashMail: async function (mail) {
    return !mail ? await this.sha256('') : await this.sha256(mail);
  },
  testForBirthdayRegex: function (birthday) {
    return /(\d{1,2})\.(\d{1,2})\.(\d{2,4})/.test(birthday);
  },
};
