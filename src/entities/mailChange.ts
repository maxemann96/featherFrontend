import { User } from './user';

export type MailChange = {
  id: string;
  user: User;
  validUntil: string;
  newMail: string;
};
