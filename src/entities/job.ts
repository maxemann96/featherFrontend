export type BackgroundJob = {
  id: string;
  created: string;
  status: string;
  completed?: string;
  exception?: string;
};
