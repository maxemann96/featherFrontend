export type User = {
  id: number;
  username: string;
  displayName: string;
  firstname: string;
  surname: string;
  mail: string;
  groups: number[];
  registeredSince: string;
  ownedGroups: number[];
  permissions: string[];
  disabled: boolean;
  lastLoginAt: string;
};
