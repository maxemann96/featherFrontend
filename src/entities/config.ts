export type Config = {
  version: string;
  bindings: BindingConfig;
  authorizations: AuthorizationConfig[];
};

export type BindingConfig = {
  nextcloud: NextcloudConfig;
  openproject: OpenProjectConfig;
  openshift: Boolean;
  multiservice: MultiServiceConfig;
};

export type NextcloudConfig = {
  enabled: boolean;
  publicUrl: string;
};

export type OpenProjectConfig = {
  enabled: boolean;
  publicUrl: string;
};

export type InactiveUserSettings = {
  reminderAfterInactivityInMonths: number;
  deactivationAfterReminderInMonths: number;
  deletionAfterDeactivationInMonths: number;
};

export type MultiServiceConfig = {
  multiServicePlugin: string;
  supportMembershipDB?: string;
  userSettings: {
    invitationRequiresGroup: boolean;
    inactiveUsersSettings: InactiveUserSettings | null;
  };
};

export type AuthorizationConfig = {
  id: string;
  type: 'CREDENTIAL' | 'OIDC' | 'TOKEN';
  name?: string;
  iconUrl?: string;
  color?: string;
};
