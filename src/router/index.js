/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import Layout from '../components/Layout.vue';
import { useApplicationStore } from '../stores/application';

import LoginRoutes from './login';
import PrintRoutes from './print';

import Invitations from "../views/users/Invitations.vue"

const DashboardLayout = () => import('../components/DashboardLayout.vue');
const Dashboard = () => import('../views/Dashboard.vue');
const Me = () => import('../views/Me.vue');
const SupportMembership = () => import('../views/SupportMembership.vue');
const PlausibilityChecks = () => import('../views/admin/PlausibilityChecks.vue');
const GroupPlausibilityChecks = () => import('../views/admin/GroupPlausibilityChecks.vue');
const SandboxStatus = () => import('../views/admin/SandboxStatus.vue');
const UploadMembershipData = () => import('../views/admin/UploadMembershipData.vue');
const Users = () => import('../views/users/Users.vue');
const AddUser = () => import('../views/users/AddUser.vue');
const ManageTrialRequests = () => import('../views/users/ManageTrialRequests.vue');
const DeleteUser = () => import('../views/users/DeleteUser.vue');
const Groups = () => import('../views/groups/Groups.vue');
const GroupDetails = () => import('../views/groups/GroupDetails.vue');
const AddGroup = () => import('../views/groups/AddGroup.vue');
const DeleteGroup = () => import('../views/groups/DeleteGroup.vue');
const Chapters = () => import('../views/admin/Chapters.vue');
const Gdpr = () => import('../views/Gdpr.vue');
const GdprAdmin = () => import('../views/admin/GdprAdmin.vue');
const AdminBlocked = () => import('../views/admin/Actions.vue');
const InactiveUsers = () => import('../views/admin/InactiveUsers.vue');
const Reactivate = () => import('../views/Reactivate.vue');

Vue.use(VueRouter);

const routes = [
  PrintRoutes,
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      LoginRoutes,
      {
        path: '/',
        component: DashboardLayout,
        children: [
          {
            path: 'dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
              miniSessionAllowed: true,
            },
          },
          {
            path: 'plausibility',
            name: 'Plausibilitätsüberprüfung',
            component: PlausibilityChecks,
          },
          {
            path: 'groupplausibility',
            name: 'Gruppenplausibilitätsüberprüfung',
            component: GroupPlausibilityChecks,
          },
          {
            path: 'uploadmembershipdb',
            name: 'Mitgliedschafts-DB',
            component: UploadMembershipData,
          },
          {
            path: 'gdpr',
            name: 'Datenschutzbestimmungen',
            component: Gdpr,
            meta: {
              miniSessionAllowed: true,
            },
          },
          {
            path: 'reactivate',
            name: 'Account reaktivieren',
            component: Reactivate,
            meta: {
              miniSessionAllowed: true,
            },
          },
          {
            path: 'sandboxStatus',
            name: 'Sandbox',
            component: SandboxStatus,
          },
          {
            path: 'me',
            name: 'Mein Profil',
            component: Me,
            meta: {
              miniSessionAllowed: true,
            },
          },
          {
            path: 'membership',
            name: 'Fördermitgliedschaft',
            component: SupportMembership,
          },
          {
            path: 'users',
            name: 'Benutzer',
            component: Users,
          },
          {
            path: 'invitations',
            name: 'Einladungen',
            component: Invitations,
          },
          {
            path: 'processtrials',
            name: 'Erhöhte Rechte Verwalten',
            component: ManageTrialRequests,
          },
          {
            path: 'adduser',
            name: 'Benutzer Anlegen',
            component: AddUser,
          },
          {
            path: 'deleteuser',
            name: 'Benutzer Löschen',
            component: DeleteUser,
            meta: {
              miniSessionAllowed: true,
            },
          },
          {
            path: 'groups',
            name: 'Gruppen',
            component: Groups,
          },
          {
            path: 'groups/:id',
            name: 'Gruppendetails',
            component: GroupDetails,
          },
          {
            path: 'addgroup',
            name: 'Gruppe Anlegen',
            component: AddGroup,
          },
          {
            path: 'deletegroup',
            name: 'Gruppe Löschen',
            component: DeleteGroup,
          },
          {
            path: 'chapters',
            name: 'Struktureinstellungen',
            component: Chapters,
          },
          {
            path: 'gdprAdmin',
            name: 'Datenschutz verwalten',
            component: GdprAdmin,
          },
          {
            path: 'admin_blocked',
            name: 'Geblockte Benutzer:innen',
            component: AdminBlocked,
          },
          {
            path: 'admin_inactive',
            name: 'Inaktive Benutzer:innen',
            component: InactiveUsers,
          },
        ],
      },
    ],
  },
  {
    path: '*',
    redirect: '/dashboard',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes,
});

router.afterEach((to) => {
  const applicationStore = useApplicationStore();
  applicationStore.clearAlerts();
  document.title = to.name;
});

export default router;
